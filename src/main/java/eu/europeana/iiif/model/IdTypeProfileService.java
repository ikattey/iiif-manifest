package eu.europeana.iiif.model;

/**
 * @author Patrick Ehlert
 * Created on 24-01-2018
 */
public class IdTypeProfileService extends JsonLdIdType {

    private static final long serialVersionUID = -4279624215412828621L;
    
    private String profile;

    public IdTypeProfileService(String id, String type) {
        super(id, type);
    }

    public String getProfile() {
        return profile;
    }

    public void setProfile(String profile) {
        this.profile = profile;
    }
}
